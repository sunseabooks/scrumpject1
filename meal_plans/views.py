from django.shortcuts import redirect, render
from django.urls import reverse_lazy
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView, DeleteView, UpdateView
from django.views.generic.list import ListView
from recipes.forms import RatingForm
from .models import MealPlan

class MealPlanCreateView(CreateView):
    model = MealPlan
    template_name = "meal_plans/new.html"
    fields = [
        "name",
        "recipes",
        "tags",
        "date",
    ]
    success_url = reverse_lazy("mealplans_list")

class MealPlanUpdateView(UpdateView):
    model = MealPlan
    template_name = "meal_plans/edit.html"
    fields = [
        "name",
        "recipes",
        "tags",
        "date",
    ]
    success_url = reverse_lazy("mealplans_list")

class MealPlanDetailView(DetailView):
    model = MealPlan
    template_name = "meal_plans/detail.html"

class MealPlanListView(ListView):
    model = MealPlan
    template_name = "meal_plans/list.html"

class MealPlanDeleteView(DeleteView):
    model = MealPlan
    template_name = "meal_plans/delete.html"
    success_url = reverse_lazy("mealplans_list")
