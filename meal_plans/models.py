from django.db import models

class MealPlan(models.Model):
    name = models.CharField(max_length=120)
    created = models.DateTimeField(auto_now_add=True)
    edited = models.DateTimeField(auto_now=True)
    date = models.DateTimeField()
    #ADD AUTH_USER AS OWNER#
    recipes = models.ManyToManyField(
        "recipes.Recipe", related_name="meal_plans")
    tags = models.ManyToManyField(
        "tags.Tag", related_name="meal_plans")
