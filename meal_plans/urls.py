from django.urls import path

from .views import (
    MealPlanCreateView,
    MealPlanDeleteView,
    MealPlanUpdateView,
    MealPlanDetailView,
    MealPlanListView,
)

urlpatterns = [
    path("<in:pk>/", MealPlanDetailView.as_view(), name="mealplan_detail"),
    path("<in:pk>/edit/", MealPlanUpdateView.as_view(), name="mealplan_edit"),
    path("<in:pk>/delete/", MealPlanDeleteView.as_view(), name="mealplan_delete"),
    path("new/", MealPlanCreateView.as_view(), name="mealplan_new"),
    path("", MealPlanListView.as_view(), name="mealplans_list"),
]
